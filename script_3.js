// Ex. 3

const user1 = {
    name: "John",
    years: 30
};

// const userValid = {user1}

const {name, years, isAdmin = false} = user1

console.log("Name: ", name)
console.log("Year: ", years)
console.log("isAdmin: ", isAdmin)