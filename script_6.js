// Ex. 6

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = {
    ...employee,
    age: 40,
    salary: 45000
};

console.log(newEmployee);
